package com.task55_30;

public class CFish extends CPet implements ISwimable {

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Fish swim");
	}
	@Override
	public void animalSound() {
		System.out.println("Fish sound");

	}

	@Override
	public void eat() {
		System.out.println("Fish eat");

	}
	@Override
	public String toString() {
		return "MyFish: [age=" + age + ", name=" + name + "]";
	}
	
}
