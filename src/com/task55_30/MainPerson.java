package com.task55_30;

import java.util.ArrayList;


public class MainPerson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CPerson myPerson = new CPerson(1, 18, "Joe2", "Smith2");
		CAnimal animal1 = new CDog();
		((CPet) animal1).setName("Haru Dog");
		((CPet) animal1).setAge(1);
		CAnimal animal2 = new CCat();
		((CPet) animal2).setName("Momo Cat");
		((CPet) animal2).setAge(2);
		CAnimal animal3 = new CBird();
		((CPet) animal3).setName("Toto Bird");
		((CPet) animal3).setAge(3);
		CAnimal animal4 = new CFish();
		((CPet) animal4).setName("Nana Fish");
		((CPet) animal4).setAge(2);
		
		ArrayList<CPet> pets = new ArrayList<CPet>();

		pets.add((CPet) animal1);

		myPerson.setPets(pets);
		myPerson.addPet((CPet) animal2);
		myPerson.addPet((CPet) animal3);
		myPerson.addPet((CPet) animal4);

		myPerson.printPerson();
		System.out.println("Pet list after remove:");
		
		myPerson.removePet(1);
		myPerson.printPerson();
	}

}
