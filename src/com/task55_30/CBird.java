package com.task55_30;

public class CBird extends CPet implements IFlyable {
	@Override
	public void animalSound() {
		System.out.println("Bird sound");

	}

	@Override
	public void eat() {
		System.out.println("Bird eat");

	}
	
	@Override
	public void fly() {
		System.out.println("Bird fly");

	}
	@Override
	public String toString() {
		return "MyBird: [age=" + age + ", name=" + name + "]";
	}
}
